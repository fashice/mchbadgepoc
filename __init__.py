import machine
import utime
import ustruct
import display
import random
import buttons
import mch22
import time
import math
# imports
from neopixel import NeoPixel
from machine import Pin

prev = 0

# Pin 19 controls the power supply to SD card and neopixels
powerPin = Pin(19, Pin.OUT)
# Pin 5 is the LED's data line
dataPin = Pin(5, Pin.OUT)
# create a neopixel object for 5 pixels
np = NeoPixel(dataPin, 5)
# turn on power to the LEDs
powerPin.on()

# Logo stuff
logostart=20
letterwidth=50
spacing=10
letterheight=200
text = "MCH2022"
font = "permanentmarker22" 
scale = 2 


# A exit
def reboot(pressed):
  if pressed:
    mch22.exit_python()

buttons.attach(buttons.BTN_A,reboot)

def drawRandomLine():
  color = random.randint(0,0xFFFFFF)
  color1 = random.randint(0,0x3F)
  color2 = random.randint(0,0x3F)
  color3 = random.randint(0,0x3F)
# Semi random colors (not too bright)
  np[0] = (color1,color2,color3)
  np[1] = (color1,color3,color2)
  np[2] = (color2,color1,color3)
  np[3] = (color2,color3,color1)
  np[4] = (color3,color1,color2)

# send colors out to LEDs
  np.write()

i2c = machine.I2C(0)
sensor = 0x28

def i2c_write_u8(sensor, reg, val):
    return i2c.writeto_mem(sensor, reg, val.to_bytes(1, 'little'))

def i2c_read_u8(sensor, reg):
    return int.from_bytes(i2c.readfrom_mem(sensor, reg, 1), 'little')

def i2c_read_s16le(sensor, reg):
    return ustruct.unpack("<h", i2c.readfrom_mem(sensor, reg, 2))[0]

def has_sensor():
    return sensor is not None

def init():
    # Select page 0
    i2c_write_u8(sensor, 0x07, 0)

    # Make sure we have the right device
    id_ = i2c_read_u8(sensor, 0x00)
    if (id_ != 0XA0):
        raise RuntimeError('Sensor not found')

    # Switch to config mode (just in case since this is the default)
    i2c_write_u8(sensor, 0X3D, 0X00)
    utime.sleep_ms(30)

    # Reset
    i2c_write_u8(sensor, 0X3F, 0x20)
    utime.sleep_ms(30)

    while True:
        try:
            if (i2c_read_u8(sensor, 0x00) == 0XA0):
                break
        except:
          utime.sleep_ms(10)

    utime.sleep_ms(50)

    # Set to normal power mode 
    i2c_write_u8(sensor, 0X3E, 0X00)
    utime.sleep_ms(10)

    i2c_write_u8(sensor, 0X3F, 0x0)
    utime.sleep_ms(10)

    i2c_write_u8(sensor, 0X3D, 0X01)
    utime.sleep_ms(20)


init()
display.drawFill(0x000000)
display.translate(display.width() / 2, display.height() / 2) # Go to the middle of the screen

while True:
    # old stuff
 #   i2c = machine.SoftI2C(sda=machine.Pin(22), scl=machine.Pin(21), freq=1000)
 #   i2c.writeto_mem(0x28, 7, b"\x01")
 #   i2c.writeto_mem(0x28, 0x3f, b"\x00")
    mystring = str(i2c.readfrom(0x28, 2, 0x08))
    rotatedata = int(i2c_read_s16le(sensor, 0X0A))

    drawRandomLine()
    display.drawFill(0x000000) # Black
    rotatedatanew=prev-rotatedata
    prev=rotatedata
    display.rotate(math.pi * rotatedatanew/2400 ) # This will rotate the screen 
    textWidth = display.getTextWidth(text, font) 
    textHeight = display.getTextHeight(text, font)
    display.pushMatrix() 
    display.scale(scale, scale) 
    display.translate(-textWidth / 2, -textHeight / 2) # Move the canvas so the text is centered
    color = random.randint(0,0xFFFFFF)
    display.drawText(0, 0, text, color, font) 
    display.popMatrix() 
    display.flush() # Flush, show everything
    time.sleep(0.1)




